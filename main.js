import Vue from "vue";
import App from "./App.vue";
import { createRouter } from "./router";
import { createStore } from "./store";
import VueMaterial from 'vue-material';
import VeeValidate from 'vee-validate';
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import { formatDate } from './filters/formatDate';
import { capitalize } from './filters/capitalize';

Vue.config.productionTip = false;

Vue.use(VueMaterial);
Vue.use(VeeValidate);
Vue.filter('formatDate', formatDate);
Vue.filter('capitalize', capitalize);

export async function createApp({
  beforeApp = () => {},
  afterApp = () => {},
} = {}) {
  const router = createRouter();
  const store = createStore();

  await beforeApp({
    router,
    store,
  });

  const app = new Vue({
    router,
    store,
    render: (h) => h(App),
  });

  const result = {
    app,
    router,
    store,
  };

  await afterApp(result);

  return result;
}
